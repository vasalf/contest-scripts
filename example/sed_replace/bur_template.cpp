#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>

using namespace std;

int main() {
#ifndef LOCAL
#define NAME "%name%"
    freopen(NAME".in", "r", stdin);
    freopen(NAME".out", "w", stdout);
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
#endif



    return 0;
}
