#!/bin/bash
#
#   mkprob.sh: make problem solution file
#   Copyright (C) 2016  Vasiliy Alferov <ya-ikmik2012@yandex.ru>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

template_path="$HOME/.local/contest-scripts/templates"

usage() {
    echo "$0: make problem solution file based on a special template"
    echo "Usage: $0 <template> <filename> <template_params>" 1>&2
    exit 1
}

if [ $# -lt 2 ]; then
    usage
fi

cmdline_opts=( $@ )
template_script=$template_path/$1
result_filename=$2
template_params=${cmdline_opts[@]:2:${#cmdline_opts}}

if [ -f $template_script ]; then
    exec $template_script $result_filename $template_params
else
    echo "$0: file $template_script not found"
    exit 1
fi


