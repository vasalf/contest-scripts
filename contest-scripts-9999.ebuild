# Copyright 1999-2016 Vasiliy Alferov
# Distributed under the terms of the GNU General Public License v3
# $Id$

EAPI=6

inherit git-r3

DESCRIPTION="platypus179 contest scripts"
HOMEPAGE="https://gitlab.com/vasalf/contest-scripts"
EGIT_REPO_URI="https://gitlab.com/vasalf/contest-scripts.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	dobin mkprob.sh
}
