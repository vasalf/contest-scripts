# contest-scripts

Here I store the contest scripts that I use.

## mkprob.sh

### Usage

`mkprob.sh <template> <filename> <template_params>`

Templates are stored in `~/.local/contest-scripts/templates/`.

### Template `sed_replace`: usage

`mkprob.sh sed_replace <filename> <subst1> <subst2> ...`

### Template `sed_replace`: add a target

Put that file into `~/.local/contest-scripts/sed_replace/`:

```bash
#!/bin/bash

cdirname="$HOME/.local/contest-scripts/sed_replace"

template_base="$cdirname/<base>"
replacements=( 'repl1' 'repl2' ... )
```

Then this will replace those strings in the template.

### Template `sed_replace`: function in `bashrc`

I store that type of stuff in `~/.bash_aliases`

```bash
mkbur() {
    if [ $# -ne 2 ]; then
        echo "usage: mkbur <filename> <problem_name>"
        return 1
    fi
    mkprob.sh sed_replace $1 bur $2
    history -d $((HISTCMD-1))
    line=$(grep -in "code here:" $1 | sed s/:.*//g)
    vim +$((line+2)) $1
}
```

## Ebuild

Put it into app-misc
